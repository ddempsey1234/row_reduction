def row_reduction(A, n, ip):
    # *some code*
    for j in range(ip+1, n):
        sf = A[j,ip]/A[ip,ip]
        for k in range(ip,n):
            A[j,k] = A[j,k] - sf*A[ip,k]
    return A